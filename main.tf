terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.11.0"
    }
  }
  backend "s3" {
    bucket = "rjl-tfstate"
    key    = "sapper-aws-deploy"
    region = "ap-southeast-2"
  }
}

provider "aws" {
  profile = "default"
  region  = "ap-southeast-2"
}

resource "aws_ecr_repository" "repository" {
  name = "inifinitycorp-registry"
}
