FROM mhart/alpine-node:12

WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN yarn

COPY rollup.config.js .
COPY tsconfig.json .
COPY static/ static/
COPY src/ src/

RUN yarn build


EXPOSE 3000
CMD ["node", "__sapper__/build"]
